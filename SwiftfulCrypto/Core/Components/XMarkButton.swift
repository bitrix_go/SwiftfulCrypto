//
//  XMarkButton.swift
//  SwiftfulCrypto
//
//  Created by Konstantin Tarasenko on 13/08/23.
//

import SwiftUI

struct XMarkButton: View {
    
    @Environment(\.presentationMode) var presentationMode
   
    
    var body: some View {
        
        Button(action: {
            dismiss()
        }, label: {
            Image(systemName: "xmark")
                .font(.headline)
        })

        


    }
    
    private func dismiss() {
      presentationMode.wrappedValue.dismiss()
    }
    
}

struct XMarkButton_Previews: PreviewProvider {
    static var previews: some View {
        XMarkButton()
    }
}
