//
//  HapticManager.swift
//  SwiftfulCrypto
//
//  Created by Konstantin Tarasenko on 16.08.2023.
//

import Foundation
import SwiftUI

class HapticManager {
    
    static let generator = UINotificationFeedbackGenerator()
    
    static func notification(type: UINotificationFeedbackGenerator.FeedbackType) {
        generator.notificationOccurred(type)
    }
    
}
