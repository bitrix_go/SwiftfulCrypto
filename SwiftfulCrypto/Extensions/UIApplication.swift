//
//  UIApplication.swift
//  SwiftfulCrypto
//
//  Created by Konstantin Tarasenko on 11/08/23.
//

import Foundation
import SwiftUI

extension UIApplication {
    
    func endEditing() {
        
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        
    }
    
}
